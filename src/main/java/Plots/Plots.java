package Plots;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.util.BlockVector;
import org.fsi.pixelcampusessentials.Pixelcampusessentials;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Plots implements CommandExecutor, Listener {

    private static final ArrayList<PlayerSelection> selectionsQeue = new ArrayList<>();
    private static Integer MAX_PLOT_SIZE;
    private static final String[] cmds = {"pos1","pos2","claim","unclaim","info","addmember","removemember"};

    public Plots(){

        try {
            MAX_PLOT_SIZE = Integer.parseInt(String.valueOf(Pixelcampusessentials.getPlugin().getConfig().getInt("max-plot-size")));
        }catch (Exception e){

            //if the value from the config fails to load, set default MAX_PLOT_SIZE to 62500;
            e.printStackTrace();
            MAX_PLOT_SIZE = 62500;
        }
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {

        if(!(sender instanceof Player)){
            sender.sendMessage("§cYou must be a Player to do this!");
            return true;
        }

        Player p = (Player)sender;
        int cmd;

        try{
            cmd = chosenCommand(args[0]);
        }catch (Exception e){
            return false;
        }

        if(cmd == -1){
            return false;
        }

        if(!sender.hasPermission("suwupremeEssentials.plot")){
            sender.sendMessage("§cYou dont have the necessary Permissions to use Plots");
            return true;
        }

        //fetching Player that will be added to the region
        Player p2 = null;
        if(cmd == 6 || cmd == 7){

            try {
                p2 = Bukkit.getPlayer(args[1]);
                if(p2 == null)throw new Exception("§cPlayer " + args[1] + " not found! Player has to be online");

            }catch (Exception e){
                p.sendMessage(e.getMessage());
                return true;
            }
        }


        try {
            switch (cmd) {
                case 1 -> setPosition(p, 1);
                case 2 -> setPosition(p, 2);
                case 3 -> claimPlot(p);
                case 4 -> unclaimPLot(p);
                case 5 -> info(p, p.getWorld(), p.getLocation());
                case 6 -> addMember(p, p2);
                case 7 -> removeMember(p, p2);
                default -> {
                    return false;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            return true;
        }

        return true;
    }

    private void setPosition(Player p, Integer posNumber){

        BlockVector playerPos = p.getLocation().toVector().toBlockVector();
        PlayerSelection sel = PlayerSelection.getfromQeue(p);

        //if player hasnt already started a selection
        if(sel == null){

            sel = new PlayerSelection(
                    p.getUniqueId().toString(),
                    posNumber == 1 ? playerPos : null,
                    posNumber == 2 ? playerPos : null
            );
        }else{

            if (posNumber == 1) {
                sel.setPos1(playerPos);
            } else {
                sel.setPos2(playerPos);
            }
        }


        selectionsQeue.add(sel);

        p.sendMessage("§aPos " + posNumber + " set.");
    }

    private void claimPlot(Player p){

        PlayerSelection sel = PlayerSelection.getfromQeue(p);

        if(sel == null){
            p.sendMessage("§cMake a Selection first using /plot [pos1,pos2]");
            return;
        }else{

            if(sel.getPos1() == null){
                p.sendMessage("§cPosition 1 is missing. Use /plot pos1");
                return;
            }else if(sel.getPos2() == null){
                p.sendMessage("§cPosition 2 is missing. Use /plot pos2");
                return;
            }
        }

        RegionManager regionManager = getRegionManager(p.getWorld());

        //Region Bounds - Expands Vertically to Build limits. (make configurable?)
        BlockVector3 corner1 = BlockVector3.at(
                sel.getPos1().getBlockX(),
                p.getWorld().getMaxHeight(),
                sel.getPos1().getBlockZ()
        );
        BlockVector3 corner2 = BlockVector3.at(
                sel.getPos2().getBlockX(),
                p.getWorld().getMinHeight(),
                sel.getPos2().getBlockZ()
        );

        //creates Plot
        ProtectedRegion newRegion = new ProtectedCuboidRegion(p.getName() + "'s_Plot", corner1, corner2);

        //convert Regions Map to List of ProtectedRegions
        List<ProtectedRegion> otherRegions = new ArrayList<>(regionManager.getRegions().values());

        //Checks for any intersecting regions
        List<String> overlappingRegions = new LinkedList<>();
        newRegion.getIntersectingRegions(otherRegions).forEach(i -> {
            overlappingRegions.add(i.getId());
        });

        if(!overlappingRegions.isEmpty()){
            p.sendMessage("§cYour Plot is overlapping with " + overlappingRegions);
            return;
        }

        if(getRegionSize(newRegion) > MAX_PLOT_SIZE){
            p.sendMessage("§cYour Plot cannot exceed " + MAX_PLOT_SIZE + " Blocks in Size");
            return;
        }


        //adds Player to its newly created Plot as owner
        DefaultDomain plotOwner = newRegion.getOwners();
        plotOwner.addPlayer(p.getUniqueId());
        newRegion.setOwners(plotOwner);

        regionManager.addRegion(newRegion);

        p.sendMessage("§aYour Plot from " + corner1 + " to " + corner2 + " is now protected!");

    }

    private void unclaimPLot(Player p){

        ProtectedRegion regionFromOwner = getRegionFromOwner(p, p.getLocation());

        if(regionFromOwner != null){
            getRegionManager(p.getWorld()).removeRegion(regionFromOwner.getId());
            p.sendMessage("§aSuccessfully removed " + regionFromOwner.getId());
            return;
        }

        p.sendMessage("§cCouldn't find a Plot that belongs to you here");
    }

    private void addMember(Player owner, Player newMember){

        ProtectedRegion regionFromOwner = getRegionFromOwner(owner, owner.getLocation());

        if(regionFromOwner != null){
            regionFromOwner.getMembers().addPlayer(newMember.getUniqueId());
            owner.sendMessage("§aAdded " + newMember.getName() + " to " + regionFromOwner.getId());
            return;
        }

        owner.sendMessage("§cYou have to be standing in your Plot as Owner");
    }

    private void removeMember(Player owner, Player oldMember){

        ProtectedRegion regionFromOwner = getRegionFromOwner(owner, owner.getLocation());

        if(regionFromOwner != null){

            if(regionFromOwner.getMembers().contains(oldMember.getUniqueId())){

                regionFromOwner.getMembers().removePlayer(oldMember.getUniqueId());
                owner.sendMessage("§aSuccessfully removed Member " + oldMember.getName() + " from " + regionFromOwner.getId());
                return;
            }else{
                owner.sendMessage("§c" + oldMember.getName() + " isn't Member of your PLot");
            }

            return;
        }

        owner.sendMessage("§cYou have to be standing in your Plot as Owner");
    }

    private void info(Player p, World w, Location location){

        ApplicableRegionSet regionAt = getRegionAt(w, location);
        for (ProtectedRegion region : regionAt) {
            p.sendMessage("§aYou are in " + region.getId() + ". It goes from " + region.getMinimumPoint() + " to " + region.getMaximumPoint());
            return;
        }

        p.sendMessage("§cThere is currently no Plot at your Location");
    }

    //get all regions the player is Standing in
    private ApplicableRegionSet getRegionAt(World w, Location location){


        BlockVector3 playerPos = BlockVector3.at(
                location.getX(),
                location.getY(),
                location.getZ()
        );

        RegionManager regionManager = getRegionManager(w);
        ApplicableRegionSet applicableRegions = regionManager.getApplicableRegions(playerPos);

        return applicableRegions;
    }

    //returns the Region the player is standing in, if he is the owner. Otherwise null
    private ProtectedRegion getRegionFromOwner(Player owner, Location location){
        ApplicableRegionSet region = getRegionAt(owner.getWorld(), location);

        for (ProtectedRegion protectedRegion : region) {
            if (protectedRegion.getOwners().contains(owner.getUniqueId())) {
                return protectedRegion;
            }
        }

        return null;
    }

    private int chosenCommand(String arg){

        for (int i = 0; i < cmds.length; i++) {
            if(arg.equalsIgnoreCase(cmds[i]))return i + 1;
        }

        return -1;
    }

    private RegionManager getRegionManager(World w){
        RegionContainer regionContainer = WorldGuard.getInstance().getPlatform().getRegionContainer();
        return regionContainer.get(BukkitAdapter.adapt(w));
    }

    private int getRegionSize(ProtectedRegion region){
        BlockVector3 maximumPoint = region.getMaximumPoint();
        BlockVector3 minimumPoint = region.getMinimumPoint();

        // Calculate the width and height of the rectangle
        int width = Math.abs(minimumPoint.getBlockX() - maximumPoint.getBlockX()) + 1;
        int height = Math.abs(minimumPoint.getBlockZ() - maximumPoint.getBlockZ()) + 1;

        // Calculate the area of the rectangle
        int area = width * height;

        return area;
    }

    public static ArrayList<PlayerSelection> getSelectionsQeue(){
        return selectionsQeue;
    }
    public static String[] getCmds(){
        return cmds;
    }

}
