package Plots;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class PlotsTabComplete implements TabCompleter {

    @Nullable
    @Override
    public List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {

        if(!(sender instanceof Player)){
            return null;
        }

        String[] cmds = Plots.getCmds();

        Player p = (Player)sender;
        List<String> tabCompletePhrases = new LinkedList<>();

        switch (args.length){
            case 0:
                break;
            case 1:
                return Arrays.stream(cmds).toList();
            case 2:

                //players should be only listed when trying to add or remove member
                if(!(args[1].equalsIgnoreCase(cmds[5]) || args[1].equalsIgnoreCase(cmds[6])))break;

                //adds all players to tab complete
                Bukkit.getServer().getOnlinePlayers().forEach(player -> {
                    tabCompletePhrases.add(player.getName());
                });

                break;
            default:
        }

        return tabCompletePhrases;
    }
}
