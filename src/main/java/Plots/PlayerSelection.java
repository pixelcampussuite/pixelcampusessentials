package Plots;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.util.BlockVector;
import org.jetbrains.annotations.NotNull;

public class PlayerSelection implements Listener {

    @EventHandler
    public void playerLeaveEvent(@NotNull PlayerQuitEvent e){
        removeFromQeue(e.getPlayer());
    }

    private boolean removeFromQeue(Player p){

        String uuidToRemove = p.getUniqueId().toString();

        for (PlayerSelection i : Plots.getSelectionsQeue()) {
            if (i.getUUID().equals(uuidToRemove)) {
                Plots.getSelectionsQeue().remove(i);
                return true;
            }
        }

        return false;
    }

    public static PlayerSelection getfromQeue(Player p){

        String uuid = p.getUniqueId().toString();

        for (PlayerSelection i : Plots.getSelectionsQeue()) {
            if (i.getUUID().equals(uuid)) {
                return i;
            }
        }

        return null;
    }

    public PlayerSelection(){
    }
    public PlayerSelection(String UUID, BlockVector pos1, BlockVector pos2) {
        this.UUID = UUID;
        this.pos1 = pos1;
        this.pos2 = pos2;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public void setPos1(BlockVector pos1) {
        this.pos1 = pos1;
    }

    public void setPos2(BlockVector pos2) {
        this.pos2 = pos2;
    }

    public String getUUID() {
        return UUID;
    }

    public BlockVector getPos1() {
        return pos1;
    }

    public BlockVector getPos2() {
        return pos2;
    }

    private String UUID = null;
    private BlockVector pos1 = null;
    private BlockVector pos2 = null;

}
