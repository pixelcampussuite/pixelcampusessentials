package org.fsi.pixelcampusessentials;

import CropHarvester.CropHarvester;
import FunAndElse.*;
import Plots.PlayerSelection;
import Plots.Plots;
import Plots.PlotsTabComplete;
import Timber.Timber;
import org.bukkit.plugin.java.JavaPlugin;

public final class Pixelcampusessentials extends JavaPlugin {

    static private Pixelcampusessentials thisPlugin;
    @Override
    public void onEnable() {

        thisPlugin = this;
        saveDefaultConfig();
        getConfig();


        getServer().getPluginManager().registerEvents(new PlayerSelection(),this);
        getServer().getPluginManager().registerEvents(new Timber(),this);
        getServer().getPluginManager().registerEvents(new SpeedLadder(),this);
        getServer().getPluginManager().registerEvents(new FreezePlayer(),this);
        getServer().getPluginManager().registerEvents(new CropHarvester(),this);
        getServer().getPluginManager().registerEvents(new KeepOutOfFlying(),this);

        try {
            getCommand("plot").setExecutor(new Plots());
            getCommand("plot").setTabCompleter(new PlotsTabComplete());
            getCommand("freeze").setExecutor(new FreezePlayer());
            getCommand("statsall").setExecutor(new PlayerStats());
            getCommand("fullbright").setExecutor(new Fullbright());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public static Pixelcampusessentials getPlugin(){
        return thisPlugin;
    }

}
