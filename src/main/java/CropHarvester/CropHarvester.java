package CropHarvester;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.block.data.Ageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.util.Objects;

public class CropHarvester implements Listener {

    @EventHandler
    public void playerRightClick(PlayerInteractEvent e){

        if(!isPermitted(e.getPlayer()))return;
        if(!Objects.equals(e.getHand(), EquipmentSlot.HAND))return;
        if(!isFullGrownCrop(e.getClickedBlock()))return;


        Block plant = e.getClickedBlock();
        Player player = e.getPlayer();


        try {
            CropCollector.collectCrops(plant,player); //Calculates Loot and puts it into Inventory or drops it

            Ageable resetCrop = (Ageable) plant.getBlockData();
            resetCrop.setAge(0); //sets the Crop back to 0 "growth". Illusion as if the Crop was re-planted. CropCollector handles the loot.

            plant.setBlockData(resetCrop);
        }catch (Exception exception){
            return;
        }

        //visual and audio effects
        plant.getWorld().playSound(plant.getLocation(), Sound.BLOCK_CROP_BREAK,1,0.8f);
        plant.getWorld().spawnParticle(Particle.BLOCK_CRACK,plant.getLocation().add(0.5,0,0.5),5,0.2,0,0.2,0,Material.COARSE_DIRT.createBlockData());
        player.swingMainHand();
    }


    //returns false on fail
    private boolean isFullGrownCrop(Block clickedBlock){

        try {
            if (!Tag.CROPS.isTagged(clickedBlock.getType()))
                return false; //check if the clicked block is a type of crop
        }catch (NullPointerException e){
            return false;
        }

        Ageable ageable = (Ageable) clickedBlock.getBlockData();

        if(ageable.getMaterial().equals(Material.MELON_STEM) || ageable.getMaterial().equals(Material.PUMPKIN_STEM)) return false; //ignore pumpkins and Melons

        return ageable.getAge() == ageable.getMaximumAge();
    }


    private boolean isPermitted(Player p){
        return
        (
                Tag.ITEMS_HOES.isTagged(p.getInventory().getItemInMainHand().getType())
        )
                && p.hasPermission("pixelcampusessentials.harvest");
    }
}
