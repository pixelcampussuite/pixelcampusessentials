package CropHarvester;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.function.BiConsumer;

public class CropCollector {

    public static void collectCrops(Block plant, Player player){

        Collection<ItemStack> drops = plant.getDrops();
        ItemStack[] droppedItems = new ItemStack[drops.size()];

        droppedItems = drops.toArray(droppedItems);

        droppedItems[1].subtract(); //index 1 contains seeds. removes one, because one is needed to be "replanted"

        for (ItemStack droppedItem : droppedItems) {


            //puts item in next free slot or sums to existing stack. addItem() returns the Items that couldnt be stored in the Inventory
            player.getInventory().addItem(droppedItem).forEach(new BiConsumer<Integer, ItemStack>() {
                @Override
                public void accept(Integer ignored, ItemStack leftoverItems) { //drops crops that didnt find space in the Inventory

                    plant.getWorld().dropItem(plant.getLocation(), leftoverItems);
                }
            });
        }


    }
    
    
}
