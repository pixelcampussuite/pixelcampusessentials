package Timber;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.fsi.pixelcampusessentials.Pixelcampusessentials;

public class Timber implements Listener {

    private static int maxRadius;

    public Timber() {
        maxRadius = Pixelcampusessentials.getPlugin().getConfig().getInt("chop-radius");
    }

    @EventHandler
    public void onDestroy(BlockBreakEvent e){
        Player p = e.getPlayer();
        World w = p.getWorld();

        if(
                p.isSneaking() &&
                Tag.ITEMS_AXES.isTagged(p.getInventory().getItemInMainHand().getType()) &&
                isLogAt(e.getBlock().getLocation())
        ){


            Location tmp = e.getBlock().getLocation();
            do{
                tmp.setY(tmp.getY()+1);
            }while(isLogAt(tmp));

            if(!isLeafAt(tmp))return;
            chopTree(e.getBlock().getLocation().add(0,1,0),e.getBlock().getWorld(),e.getBlock().getX(),e.getBlock().getZ());
        }
    }

    private void chopTree(Location pos, World w, int startX, int startZ){

        if(!(isLogAt(pos) || isLeafAt(pos)))return;

        new BukkitRunnable() {
            //final BlockData d1 = w.getBlockAt(pos).getBlockData();

            @Override
            public void run() {

                boolean isLog = isLogAt(pos);
                if(!w.getBlockAt(pos).breakNaturally(new ItemStack(Material.DIAMOND_AXE)) ||
                        (pos.getBlockX() > startX+maxRadius || pos.getBlockX() < startX-maxRadius) || (pos.getBlockZ() > startZ+maxRadius || pos.getBlockZ() < startZ-maxRadius))return;

                if(isLog)w.playSound(pos, Sound.BLOCK_WOOD_BREAK, 1,1);
                else w.playSound(pos, Sound.BLOCK_GRASS_PLACE, 1,1);

                //w.spawnParticle(Particle.BLOCK_CRACK, pos.getX()+0.5,pos.getY()+0.9,pos.getZ()+0.5, 20, 0.25, -0.1, 0.25, 0, false);

                chopTree(pos.clone().add(1,0,0),w,startX,startZ);
                chopTree(pos.clone().add(-1,0,0),w,startX,startZ);
                chopTree(pos.clone().add(1,1,0),w,startX,startZ);
                chopTree(pos.clone().add(-1,1,0),w,startX,startZ);
                chopTree(pos.clone().add(1,-1,0),w,startX,startZ);
                chopTree(pos.clone().add(-1,-1,0),w,startX,startZ);

                chopTree(pos.clone().add(0,1,0),w,startX,startZ);
                chopTree(pos.clone().add(0,-1,0),w,startX,startZ);

                chopTree(pos.clone().add(0,0,1),w,startX,startZ);
                chopTree(pos.clone().add(0,0,-1),w,startX,startZ);
                chopTree(pos.clone().add(0,1,1),w,startX,startZ);
                chopTree(pos.clone().add(0,1,-1),w,startX,startZ);
                chopTree(pos.clone().add(0,-1,1),w,startX,startZ);
                chopTree(pos.clone().add(0,-1,-1),w,startX,startZ);
            }

        }.runTaskLater(Pixelcampusessentials.getPlugin(),4);
    }


    private boolean isLogAt(Location pos){
        return pos.getWorld() != null && Tag.LOGS.isTagged(pos.getWorld().getBlockAt(pos).getType()); //(pos.getWorld().getBlockAt(pos).getType().toString().contains("LOG") || pos.getWorld().getBlockAt(pos).getType().toString().contains("STEM"));
    }

    private boolean isLeafAt(Location pos){
        return pos.getWorld() != null && Tag.MINEABLE_HOE.isTagged(pos.getWorld().getBlockAt(pos).getType());//(pos.getWorld().getBlockAt(pos).getType().toString().contains("LEAVES") || pos.getWorld().getBlockAt(pos).getType().toString().contains("WART_BLOCK"));
    }

    private boolean blockContains(Location pos, String s){
        return pos.getWorld() != null && pos.getWorld().getBlockAt(pos).getType().toString().contains(s);
    }
}