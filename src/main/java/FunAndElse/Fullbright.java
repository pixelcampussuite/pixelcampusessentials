package FunAndElse;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.jetbrains.annotations.NotNull;

public class Fullbright implements CommandExecutor {


    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {

        if(sender instanceof Player p){

            for (PotionEffect activePotionEffect : p.getActivePotionEffects()) {
                if(activePotionEffect.getType().equals(PotionEffectType.NIGHT_VISION)){
                    p.removePotionEffect(PotionEffectType.NIGHT_VISION);
                    return true;
                }
            }

            p.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION,PotionEffect.INFINITE_DURATION,0,true,false,false));
        }

        return false;
    }
}