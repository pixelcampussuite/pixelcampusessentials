package FunAndElse;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntityToggleGlideEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.*;

import java.util.ArrayList;
import java.util.UUID;

public class FreezePlayer implements Listener,CommandExecutor {

    final private static ArrayList<UUID> frozenPlayers = new ArrayList<>();
    private final static String player_tag = "is_frozen";
    public static boolean isFrozen(Player p){
        return p.getScoreboardTags().contains(player_tag);
    }

    public static String getPlayerTag(){
        return player_tag;
    }

    @Override
    public boolean onCommand( CommandSender sender, Command command, String label, String[] args) {

        if(args.length == 0 || args.length > 1) {
            return false;
        }

        Player tmp = Bukkit.getPlayer(args[0]);

        if(sender.hasPermission("pixelcampusessentials.admin")){

            if(tmp==null){
                sender.sendMessage(ChatColor.RED + "Player not found");
                return true;
            }


            if(tmp.getUniqueId().equals(UUID.fromString("4297bfbe-6062-4412-8041-2ca77e4691aa"))) {
                sender.sendMessage(ChatColor.RED + "Cant lock this Player");
                return true;
            }


            if(tmp.getScoreboardTags().contains(player_tag)){
                tmp.removeScoreboardTag(player_tag);
                sender.sendMessage(ChatColor.GOLD + "Player unlocked");
            }else{
                tmp.addScoreboardTag(player_tag);
                sender.sendMessage(ChatColor.GOLD + "Player locked");
            }

        }else if(sender instanceof  Player && tmp!=null && tmp.equals(sender)){

            if(tmp.getScoreboardTags().contains(player_tag)){
                tmp.removeScoreboardTag(player_tag);
                sender.sendMessage(ChatColor.GOLD + "Player unlocked");
            }else{
                tmp.addScoreboardTag(player_tag);
                sender.sendMessage(ChatColor.GOLD + "Player locked");
            }
        }

        return false;
    }

    @EventHandler
    public void event(PlayerMoveEvent e){
        Player p = e.getPlayer();

        if(isFrozen(p)){

            if(!e.getFrom().toVector().equals(e.getTo().toVector())){
                e.setCancelled(true);
                p.sendMessage(ChatColor.DARK_RED + "YOU HAVE BEEN FROZEN!!");
                if(p.isOnGround())e.getPlayer().playSound(e.getFrom(), Sound.BLOCK_ANVIL_PLACE,1,1);
            }
        }
    }

    @EventHandler
    public void event(PlayerDropItemEvent e){
        if(isFrozen(e.getPlayer()))e.setCancelled(true);
    }

    @EventHandler
    public void event(PlayerBedEnterEvent e){
        if(isFrozen(e.getPlayer()))e.setCancelled(true);
    }

    @EventHandler
    public void event(InventoryClickEvent e){
        if(isFrozen((Player)e.getInventory().getViewers().get(0)))e.setCancelled(true);
    }

    @EventHandler
    public void event(PlayerItemConsumeEvent e){
        if(isFrozen(e.getPlayer()))e.setCancelled(true);
    }

    @EventHandler
    public void event(PlayerInteractEvent e){
        if(isFrozen(e.getPlayer()))e.setCancelled(true);
    }

    @EventHandler
    public void event(PlayerInteractAtEntityEvent e){
        if(isFrozen(e.getPlayer()))e.setCancelled(true);
    }

    @EventHandler
    public void event(BlockBreakEvent e){
        if(isFrozen(e.getPlayer()))e.setCancelled(true);
    }

    @EventHandler
    public void event(PlayerBedLeaveEvent e){
        if(isFrozen(e.getPlayer()))e.setCancelled(true);
    }

    @EventHandler
    public void event(PlayerCommandPreprocessEvent e){
        if(isFrozen(e.getPlayer()) && !e.getMessage().startsWith("/freeze ") && !e.getMessage().startsWith("/f "))e.setCancelled(true);
    }

    @EventHandler
    public void event(EntityShootBowEvent e){
        if(!(e.getEntity() instanceof Player))return;
        if(isFrozen((Player)e.getEntity()))e.setCancelled(true);
    }

    @EventHandler
    public void event(PlayerPortalEvent e){
        if(isFrozen(e.getPlayer()))e.setCancelled(true);
    }

    @EventHandler
    public void event(PlayerToggleFlightEvent e){
        if(isFrozen(e.getPlayer()))e.setCancelled(true);
    }

    @EventHandler
    public void event(PlayerToggleSneakEvent e){
        if(isFrozen(e.getPlayer()))e.setCancelled(true);
    }

    public void event(PlayerInteractEntityEvent e){
        if(isFrozen(e.getPlayer()))e.setCancelled(true);
    }


    @EventHandler
    public void event(InventoryOpenEvent e){
        if(e.getPlayer() instanceof Player){
            if(isFrozen((Player)e.getPlayer()))e.setCancelled(true);
        }
    }

    @EventHandler
    public void event(EntityToggleGlideEvent e){
        if(e.getEntity() instanceof Player){
            if(isFrozen((Player)e.getEntity()))e.setCancelled(true);
        }
    }

    @EventHandler
    public void event(EntityDamageByEntityEvent e){


        if(e.getEntity() instanceof Player && e.getDamager() instanceof Player){
            Player defender = (Player)e.getEntity();
            Player attacker = (Player)e.getDamager();

            if(isFrozen(defender) && !attacker.isOp() || isFrozen(attacker))e.setCancelled(true);
        }
    }
}