package FunAndElse;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Statistic;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class PlayerStats implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(label.equalsIgnoreCase("statsall"))sender.sendMessage(ChatColor.GRAY + "please wait..");

        String playerNameColor = ChatColor.GRAY.toString() + ChatColor.BOLD.toString(); //§7§l
        StringBuilder toSend = new StringBuilder();

        sender.sendMessage("");//new line
        for(OfflinePlayer p2 : sender.getServer().getOfflinePlayers()){

            if(!p2.isOnline() && label.equalsIgnoreCase("stats"))continue; //doesnt display offline players if "only" stats is typed

            String rawName = "";
            String name = "";


            try {
                rawName = p2.getName();
                name = playerNameColor + rawName;

                if(p2.isOnline())name = ChatColor.GREEN + "⬤ " + name; //§a
                else name = ChatColor.RED + "⬤ " + name; //§4

                //name += getSpaces(18 - rawName.length());

            }catch(NullPointerException e1){}


            toSend.append(name).append(formatedStats(p2)).append("\n"); //"§8: "
        }

        sender.sendMessage(toSend.toString());
        return false;
    }


    private String formatedStats(OfflinePlayer p){

        int sec = 0,min = 0,hrs = 0;
        int deaths = 0;
        int distance = 0;
        int timeSinceDeath = 0;

        String numberColor= ChatColor.GREEN.toString(); //§a
        String textColor=ChatColor.GRAY.toString(); //§7

        sec = p.getStatistic(Statistic.TOTAL_WORLD_TIME)/20; //getStatistic(Statistic.TOTAL_WORLD_TIME)
        deaths = p.getStatistic(Statistic.DEATHS);
        distance = (p.getStatistic(Statistic.WALK_ONE_CM)/100) + (p.getStatistic(Statistic.SPRINT_ONE_CM)/100) +
                (p.getStatistic(Statistic.SWIM_ONE_CM)/100) + (p.getStatistic(Statistic.BOAT_ONE_CM) +
                (p.getStatistic(Statistic.WALK_UNDER_WATER_ONE_CM)/100) + (p.getStatistic(Statistic.WALK_ON_WATER_ONE_CM)/100) +
                (p.getStatistic(Statistic.CROUCH_ONE_CM)/100) + (p.getStatistic(Statistic.FALL_ONE_CM)/100) +
                (p.getStatistic(Statistic.AVIATE_ONE_CM)/100) + (p.getStatistic(Statistic.FLY_ONE_CM)/100)+
                (p.getStatistic(Statistic.HORSE_ONE_CM)/100) + (p.getStatistic(Statistic.MINECART_ONE_CM)/100)+
                (p.getStatistic(Statistic.PIG_ONE_CM)/100) + (p.getStatistic(Statistic.STRIDER_ONE_CM)/100) +
                (p.getStatistic(Statistic.CLIMB_ONE_CM)/100));
        timeSinceDeath = p.getStatistic(Statistic.TIME_SINCE_DEATH);

        min = (sec/60) % 60;
        hrs = sec/3600;
        sec %= 60;

        //§r                                                                                                    §3| §c
        return  ": "+numberColor+hrs+textColor+"h "+numberColor+min+textColor+"min "+numberColor+sec+textColor+"sek Playtime, "+
                ChatColor.RED+deaths+textColor+" Death"+(deaths!=1?"s":"") + ",\n" +
                ChatColor.RED+timeSinceDeath/72000+textColor+"h "+ChatColor.RED+((timeSinceDeath/1200) % 60) +textColor +"m Playime after last Death, " +
                ChatColor.RED+((float)distance)/1000.0+textColor+"km Traveled total";
    }

    private String getSpaces(int amount){
        if(amount < 0)return "";
        return " ".repeat(amount);
    }
}
