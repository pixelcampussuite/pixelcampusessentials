package FunAndElse;

import org.bukkit.Particle;
import org.bukkit.Tag;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.util.Vector;

public class SpeedLadder implements Listener {

    @EventHandler
    private void onLadder(PlayerMoveEvent e){
        Player p = e.getPlayer();

        if(!p.hasPermission("pixelcampusessentials.ladder"))return;


        if(Tag.CLIMBABLE.isTagged(p.getWorld().getBlockAt(p.getLocation().add(0,1,0)).getType()) && p.isSneaking()){
            int pitch = (int)p.getLocation().getPitch();
            p.setGliding(false);

            if(pitch < -40){
                p.setVelocity(new Vector(0,0.7,0));
                p.getWorld().spawnParticle(Particle.CLOUD, p.getLocation(),2, 0, 0, 0, 0, null, true);
            }
        }
    }

    @EventHandler
    public void playerToggleSneak(PlayerToggleSneakEvent e){
        Player p = e.getPlayer();

        if(!p.hasPermission("pixelcampusessentials.ladder"))return;

        if(!p.isSneaking() && p.isClimbing()){
            if(p.getLocation().getPitch() < -40)p.setVelocity(new Vector(0,0.6,0));
        }
    }
}