package FunAndElse;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class KeepOutOfFlying implements Listener {

    @EventHandler
    public void playerJoin(PlayerJoinEvent e){

        Player player = e.getPlayer();

        if(player.hasPermission("pixelcampusessentials.admin"))return;

        if(player.getGameMode().equals(GameMode.SURVIVAL) || player.getGameMode().equals(GameMode.ADVENTURE)){
            player.setFlying(false);
        }
    }
}
